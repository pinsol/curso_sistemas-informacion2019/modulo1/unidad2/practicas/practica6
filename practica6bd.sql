﻿                                            /** MODULO I - UNIDAD II - PRACTICA 6 **/

-- creando la base de datos
DROP DATABASE IF EXISTS practica6;
CREATE DATABASE IF NOT EXISTS practica6; 
USE practica6;

-- creando las tablas
CREATE OR REPLACE TABLE autor(
  dni varchar(10),
  nombre varchar(15),
  universidad varchar(50),

  PRIMARY KEY (dni)
);

CREATE OR REPLACE TABLE tema(
  codtema char(2),
  descripcion varchar(150),

  PRIMARY KEY (codtema)
);

CREATE OR REPLACE TABLE revista(
  referencia char(3),
  titulo_rev varchar(15),
  editorial varchar(15),

  PRIMARY KEY (referencia)
);

CREATE OR REPLACE TABLE articulo(
  referencia char(3),
  dni varchar(10),
  codtema char(2),
  titulo_art varchar(50),
  anno int,
  volumen char(3),
  numero char(5),
  paginas char(4),

  PRIMARY KEY (referencia,dni,codtema)
);

-- creando claves ajenas
ALTER TABLE articulo
ADD CONSTRAINT fkarticulorevista
FOREIGN KEY (referencia) REFERENCES revista(referencia);

ALTER TABLE articulo
ADD CONSTRAINT fkarticuloautor
FOREIGN KEY (dni) REFERENCES autor(dni);

ALTER TABLE articulo
ADD CONSTRAINT fkarticulotema
FOREIGN KEY (codtema) REFERENCES tema(codtema);