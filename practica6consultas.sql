﻿                                              /** MODULO I - UNIDAD II - PRACTICA 6 **/

                                                           /* CONSULTAS */
USE practica6;
-- 1.-
SELECT c2.titulo_art FROM 
(
  SELECT * FROM tema t WHERE t.descripcion = 'Bases de datos'
) c1
JOIN
(
  SELECT * FROM articulo a WHERE a.anno = 1990
) c2
USING (codtema)
;

  -- optimizando la consulta
SELECT c2.titulo_art FROM 
(
  SELECT t.codtema FROM tema t WHERE t.descripcion = 'Bases de datos'
) c1
JOIN
(
  SELECT a.titulo_art, a.codtema FROM articulo a WHERE a.anno = 1990
) c2
USING (codtema)
;